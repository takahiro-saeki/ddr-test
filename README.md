Overview  
====

Materialize sample  


## Description

this is a sample of the [materialize](http://materializecss.com/) of CSS Framework.  
Please look at the [materialize](http://materializecss.com/) in advance.  


## Contribution

1. Fork it ( https://github.com/takahiro-saeki/ddr-test/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create new Pull Request


## Licence

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)


## Author

[materialize](http://materializecss.com/)

